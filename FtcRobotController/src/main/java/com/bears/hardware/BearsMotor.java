package com.bears.hardware;

import android.graphics.Path;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Range;


/**
 * Awesomely created by lord_alonso on 14/12/15.
 */
public class BearsMotor{
    public HardwareMap hardwareMap;
    public DcMotor motor;
    public Type type;


    public BearsMotor(String name, Type type, HardwareMap hardwareMap){
        this(name, type, DcMotor.Direction.FORWARD, hardwareMap);
    }
    public BearsMotor(String name, Type type, DcMotor.Direction direction, HardwareMap hardwareMap){
        this.hardwareMap = hardwareMap; //  TEST!!! >> new HardwareMap();
        this.motor = hardwareMap.dcMotor.get(name);
        this.motor.setDirection(direction);
        this.type = type;
    }

    public BearsMotor(DcMotor motor, Type type){
        this(motor, type, DcMotor.Direction.FORWARD);
    }
    public BearsMotor(DcMotor motor, Type type, DcMotor.Direction direction){
        this.motor = motor;
        this.motor.setDirection(direction);
        this.type = type;
    }

    public enum Type {
        TETRIX(1440),
        NEVEREST20(560),
        NEVEREST40(-1120),
        NEVEREST60(1680);

        public int ppr;

        Type(int ppr){
            this.ppr = ppr;
        }
    }

    public void setPower(double power){
        motor.setPower(Range.clip(power, -1, 1));
    }

}
