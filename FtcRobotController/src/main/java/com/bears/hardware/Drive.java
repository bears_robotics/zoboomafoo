package com.bears.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Range;

import java.lang.Math;

/**
 * Awesomely created by lord_alonso on 5/09/15.
 */

public class Drive {
    private BearsMotor rightFront = null;
    private BearsMotor rightBack = null;
    private BearsMotor leftFront = null;
    private BearsMotor leftBack = null;
    private Gamepad gamepad = null;

    private BearsMotor.Type type;

    private double xPower;
    private double yPower;
    private double turnPower;

    private double joyY;
    private double joyX;
    private double joyY2;
    private double joyX2;

    private double[] outputs;

    private double deadband;
    private boolean smoothModeEnabled;
    private boolean scaleModeEnabled;
    private double precisionMultiplier;
    private double currentPrecisionMultiplier;
    private double wheelRadiusIn;
    private double gearRatio;

    public boolean gotToPosition;
    public boolean slowToggled = false;

    public Drive(HardwareMap hardwareMap, BearsMotor.Type type, Gamepad gamepad){
        this(hardwareMap, type, gamepad,0.0, false, false, 4, 1);
    }

    public Drive(HardwareMap hardwareMap, BearsMotor.Type type, Gamepad gamepad, double deadband, boolean smooth, boolean scale, double wheelRadiusIn, double gearRatio){
        this.rightFront = new BearsMotor("rightFront", type, hardwareMap);
        this.rightBack = new BearsMotor("rightBack", type, hardwareMap);
        this.leftFront= new BearsMotor("leftFront", type, DcMotor.Direction.REVERSE, hardwareMap);
        this.leftBack = new BearsMotor("leftBack", type, DcMotor.Direction.REVERSE, hardwareMap);

        this.gamepad = gamepad;
        this.type = type;
        this.deadband = deadband;
        this.smoothModeEnabled = smooth;
        this.scaleModeEnabled = scale;
        this.precisionMultiplier = (double)0.5;
        this.currentPrecisionMultiplier = 1;
        this.wheelRadiusIn = wheelRadiusIn;
        this.gearRatio = gearRatio;
    }

    public Drive(BearsMotor rightFront, BearsMotor rightBack, BearsMotor leftFront, BearsMotor leftBack, Gamepad gamepad){
        this(rightFront, rightBack, leftFront, leftBack, gamepad,(double)0.0,false,false);
    }

    public Drive(BearsMotor rightFront, BearsMotor rightBack, BearsMotor leftFront, BearsMotor leftBack, Gamepad gamepad, double deadband, boolean smooth, boolean scale){
        this(rightFront, rightBack, leftFront, leftBack, gamepad, deadband, smooth, scale, 4, 1);
    }

    public Drive(BearsMotor rightFront, BearsMotor rightBack, BearsMotor leftFront, BearsMotor leftBack, Gamepad gamepad, double deadband, boolean smooth, boolean scale, double wheelRadiusIn, double gearRatio){
        this.rightFront = rightFront;
        this.rightBack = rightBack;
        this.leftFront= leftFront;
        this.leftBack = leftBack;

        this.leftFront.motor.setDirection(DcMotor.Direction.REVERSE);
        this.leftBack.motor.setDirection(DcMotor.Direction.REVERSE);

        this.gamepad = gamepad;
        this.deadband = deadband;
        this.smoothModeEnabled = smooth;
        this.scaleModeEnabled = scale;
        this.precisionMultiplier = 0.4;
        this.currentPrecisionMultiplier = 1;
        this.wheelRadiusIn = wheelRadiusIn;
        this.gearRatio = gearRatio;
    }


    public void drive(double yPower, double xPower, double turnPower){
        outputs = new double[4];
        outputs[0] = yPower - xPower - turnPower;
        outputs[1] = yPower + xPower - turnPower;
        outputs[2] = yPower + xPower + turnPower;
        outputs[3] = yPower - xPower + turnPower;
        tuneOutputs(outputs);

        rightFront.setPower(outputs[0]);
        rightBack.setPower(outputs[1]);
        leftFront.setPower(outputs[2]);
        leftBack.setPower(outputs[3]);
    }
    /*----------------------------------------Drives---------------------------------------------*/

    public void tankdrive(){
        updateJoysticks();
        joyY = tuneInput(joyY);
        joyY2 = tuneInput(joyY2);

        //tuneInputs(inputs);

        yPower = joyY + joyY2;        //
        xPower = 0.0;                 // MUST SCALE!
        turnPower = joyY - joyY2;     //

        scaleModeEnabled = true;
        drive(yPower, xPower, turnPower);
    }

    public void arcadeDrive(){

        updateJoysticks();
        joyY = tuneInput(joyY);
        joyX2 = tuneInput(joyX2);
        //tuneInputs(inputs);

        yPower = joyY;
        xPower = 0.0;
        turnPower = joyX2;

        drive(yPower, xPower, turnPower);
    }

    public void omniDrive(){
        updateJoysticks();
        joyY = tuneInput(joyY);
        joyX = tuneInput(joyX);
        joyX2 = tuneInput(joyX2);
        //tuneInputs(inputs);

        yPower = joyY;
        xPower = joyX;
        turnPower = joyX2;

        drive(yPower, xPower, turnPower);
    }

    /*-----------------------------------Input & Output Management-----------------------------------------*/

    private void updateJoysticks(){

        joyY = -gamepad.left_stick_y;
        joyX = gamepad.left_stick_x;
        joyY2 = -gamepad.right_stick_y;
        joyX2 = gamepad.right_stick_x;
    }

    private double tuneInput(double input){
        input = tuneDeadband(input);
        if (smoothModeEnabled) {
            input = tuneSmooth(input);
        }

        return input;
    }

    private void tuneOutputs(double[] outputs){
        if(scaleModeEnabled)
            scale(outputs);
        tunePrecision(outputs);
    }



    private double tuneDeadband(double input) {
            int sig = (int) Math.signum(input);
            if (Math.abs(input) < deadband) {
                return 0;
            }
            else {
                return (input - deadband * sig) / (1 - deadband);
            }
    }


    private double tuneSmooth(double input){
        if(input==0)
            return 0;
        else
            return  Math.pow(input, 2) * input/Math.abs(input);
    }


    private void tunePrecision(double[] outputs){
        for (int i=0; i < outputs.length; i++)
            outputs[i] = outputs[i] * currentPrecisionMultiplier;
    }


    private void scale(double[] outputs){                  // Beta Function
        double currentLargest = 0;
        for (double output : outputs) {                    //
            if (Math.abs(output) > currentLargest)         // Get largest absolute value in output
                currentLargest = Math.abs(output);         //
        }

        if (currentLargest != 0 && currentLargest > 1.0) {
            for (int i = 0; i < outputs.length; i++) {     //
                outputs[i] = outputs[i] / currentLargest;  // Scale all other values so max is 1
            }                                              //
        }
    }




/*___________________________________________Auto________________________________________________*/


    public void driveToPosition(double distanceInCm, double yPower, double xPower, double turnPower){
        int targetTicks =(int) ( distanceInCm / ( Math.PI * 2 * (wheelRadiusIn * 2.54)) * rightBack.type.ppr * gearRatio );


            drive(yPower, xPower, turnPower);

            if (targetTicks - Math.abs(rightBack.motor.getCurrentPosition()) < targetTicks*.5  && !slowToggled ){
                togglePrecision();
                slowToggled = true;
            }
        int currentPosition = Math.abs( rightBack.motor.getCurrentPosition() );

        if(currentPosition >= targetTicks) {
            drive(0, 0, 0);
            gotToPosition = true;
        }
    }


    public void resetDrive(){
        rightFront.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        rightBack.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        leftFront.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        leftBack.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);

        slowToggled = false;
        gotToPosition = false;
    }

    public void readyDrive(){
        rightFront.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        rightBack.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        leftFront.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        leftBack.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
    }


    public void driveToPositionDefault(double distanceInCm, double yPower, double turnPower){

        int targetTicks =(int) ( distanceInCm / ( Math.PI * 2*(wheelRadiusIn * 2.54)) * rightFront.type.ppr * gearRatio);

        int direction1 = (int) Math.signum(yPower - turnPower);
        int direction2 = (int) Math.signum(yPower + turnPower);

        rightFront.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        rightBack.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        leftFront.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        leftBack.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);

        rightFront.motor.setTargetPosition(targetTicks*direction1);
        rightBack.motor.setTargetPosition(targetTicks*direction1);
        leftFront.motor.setTargetPosition(targetTicks*direction2);
        leftBack.motor.setTargetPosition(targetTicks*direction2);

        rightFront.setPower(yPower  - turnPower);
        rightBack.setPower(yPower - turnPower);
        leftFront.setPower(yPower + turnPower);
        leftBack.setPower(yPower + turnPower);

        if ( Math.abs(rightFront.motor.getCurrentPosition()) >= Math.abs(targetTicks) || Math.abs(rightBack.motor.getCurrentPosition()) >= Math.abs(targetTicks) ){
            gotToPosition=true;
        }

    }


    public void driveToPositionDefault(double distanceInCm, double yPower, double xPower, double turnPower){  //  EXPERIMENTAL!!
        double RFPower = Range.clip((yPower - xPower - turnPower), -1, 1);
        double RBPower = Range.clip((yPower + xPower - turnPower), -1, 1);
        double LFPower = Range.clip((yPower + xPower + turnPower), -1, 1);
        double LBPower = Range.clip((yPower - xPower + turnPower), -1, 1);

        int RFTarget =(int) ( ( distanceInCm / ( Math.PI * 2*(wheelRadiusIn * 2.54)) * rightFront.type.ppr) * RFPower );
        int RBTarget =(int) ( ( distanceInCm / ( Math.PI * 2*(wheelRadiusIn * 2.54)) * rightBack.type.ppr) * RBPower );
        int LFTarget =(int) ( ( distanceInCm / ( Math.PI * 2*(wheelRadiusIn * 2.54)) * leftFront.type.ppr) * LFPower );
        int LBTarget =(int) ( ( distanceInCm / ( Math.PI * 2*(wheelRadiusIn * 2.54)) * leftBack.type.ppr) * LBPower );
        boolean toggled = false;

        rightFront.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        rightBack.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        leftFront.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        leftBack.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);

        rightFront.motor.setTargetPosition(RFTarget);
        rightBack.motor.setTargetPosition(RBTarget);
        leftFront.motor.setTargetPosition(LFTarget);
        leftBack.motor.setTargetPosition(LBTarget);

        rightFront.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        rightBack.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        leftFront.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);
        leftBack.motor.setMode(DcMotorController.RunMode.RUN_TO_POSITION);

        rightFront.setPower(RFPower);
        rightBack.setPower(RBPower);
        leftFront.setPower(LFPower);
        leftBack.setPower(LBPower);

    }



/*____________________________________Getters and Setters________________________________________*/


    //Setter and Getter methods for Smooth(squared) Mode
    public boolean getSmoothModeEnabled() {
        return smoothModeEnabled;
    }

    public void setSmoothModeEnabled(boolean smoothModeEnabled) {
        this.smoothModeEnabled = smoothModeEnabled;
    }

    //Setter and Getter methods for Scale Mode
    public boolean getScaleModeEnabled() {
        return scaleModeEnabled;
    }

    public void setScaleModeEnabled(boolean scaleModeEnabled) {
        this.scaleModeEnabled = scaleModeEnabled;
    }

    //Setter and Getter methods for Deadband
    public double getDeadband() {
        return deadband;
    }

    public void setDeadband(double deadband) {
        this.deadband = deadband;
    }

    //Setter and Getter methods for Precision Multiplier
    public double getPrecisionMultiplier() {
        return precisionMultiplier;
    }

    public void setPrecisionMultiplier(double precisionMultiplier) {
        this.precisionMultiplier = precisionMultiplier;
    }

    //Setter and Getter methods for Wheel Radius
    public double getWheelRadiusIn() {
        return wheelRadiusIn;
    }

    public void setWheelRadiusIn(double wheelRadiusIn) {
        this.wheelRadiusIn = wheelRadiusIn;
    }

    //Getter method for Precision Current Multiplier
    public double getCurrentPrecisionMultiplier(){
        return currentPrecisionMultiplier;
    }

    //Toggler method for Precision
    public void togglePrecision(){
        currentPrecisionMultiplier = -currentPrecisionMultiplier + precisionMultiplier + 1;
    }
    public void resetPrecision(){
        currentPrecisionMultiplier = 1;
    }


    public double getxPower() {
        return xPower;
    }

    public double getyPower() {
        return yPower;
    }

    public double getTurnPower() {
        return turnPower;
    }

    public double getJoyY() {
        return joyY;
    }

    public double getJoyX() {
        return joyX;
    }

    public double getJoyY2() {
        return joyY2;
    }

    public double getJoyX2() {
        return joyX2;
    }


    public String string(){
        return  String.format("X Power: %f\n", xPower)+
                String.format("Y Power: %f\n", yPower)+
                String.format("Turn Power: %f\n", turnPower)+
                String.format("Outputs 0: %f\n", outputs[0])+
                String.format("Outputs 1: %f\n", outputs[1])+
                String.format("Outputs 2: %f\n", outputs[2])+
                String.format("Outputs 3: %f\n", outputs[3])+
                String.format("Joy Y: %f\n", joyY)+
                String.format("Joy X: %f\n", joyX)+
                String.format("Joy Y2: %f\n", joyY2)+
                String.format("Joy X2: %f\n", joyX2)+
                String.format("Precision Multiplier %f\n", currentPrecisionMultiplier);

    }
}