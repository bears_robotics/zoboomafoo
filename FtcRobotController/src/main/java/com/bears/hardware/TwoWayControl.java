package com.bears.hardware;


/**
 * Created by Alonso on 13/01/2016.
 */
public class TwoWayControl {
    BearsMotor motor1;
    public int x = 1;
    //BearsMotor motor2;
    boolean safety = true;
    double max;
    double min;
    double counter;

    public TwoWayControl(BearsMotor motor1, BearsMotor motor2){
        this.motor1 = motor1;
        //this.motor2 = motor2;

        this.safety = false;
    }
    public TwoWayControl(BearsMotor motor1, BearsMotor motor2, double min, double max){
        this.motor1 = motor1;
        //this.motor2 = motor2;

        this.min = min;
        this.max = max;
    }

    public void updateCounter(double counter){
        this.counter = counter;
    }

    public void setPower(double power){
        if( (counter > min && counter < max) || !safety) {
            motor1.setPower(power);
            //motor2.setPower(power);
        }
        else{
            motor1.setPower(0);
            //motor2.setPower(0);

        }
    }




}
