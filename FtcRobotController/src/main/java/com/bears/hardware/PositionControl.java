package com.bears.hardware;

import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by Alonso on 13/01/2016.
 */
public class PositionControl {

    double kp = .001;
    double ki = .000005;
    double p;
    double i;

    double startPosition;
    double currentPosition;
    double realPosition;

    double targetPosition;
    public double error;
    double sumError;

    int range = 100;

    double max;
    double min;
    public boolean safety = false; /// REMOVE PUBLIC!!!!

    public BearsMotor motor1; /// REMOVE PUBLIC!!!!
    BearsMotor motor2;

    public PositionControl(BearsMotor motor1, BearsMotor motor2, double min, double max){
        this.motor1 = motor1;
        //this.motor2 = motor2;
        this.max = max;
        this.min = min;
        this.safety = true;
    }
    public PositionControl(BearsMotor motor1, BearsMotor motor2){
        this.motor1 = motor1;
        //this.motor2 = motor2;
        this.safety = false;
    }

    //public PositionControl(BearsMotor motor1, BearsMotor motor2) {
    //    super(motor1, motor2);
    //}
    //public PositionControl(BearsMotor motor1, BearsMotor motor2, double min, double max){
    //    super(motor1, motor2, min, max);
    //}

    public void init(){
        motor1.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        motor1.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        startPosition = motor1.motor.getCurrentPosition();
    }

    public void reset(){
        startPosition = currentPosition;
        realPosition = currentPosition - startPosition;
        targetPosition = realPosition;
        sumError = 0;
    }


    public void setPower(double power){
        if( (  (realPosition >= max && power > 0) || (realPosition <= min && power < 0)  )   &&   safety ){
            motor1.setPower(0);
            //motor2.setPower(0);
        }
        else{
            motor1.setPower(power);
            //motor2.setPower(power);
        }
    }


    public void updatePosition(){
        currentPosition = motor1.motor.getCurrentPosition();
        realPosition = currentPosition - startPosition;
    }


    public void moveToPosition(){
        error = targetPosition - realPosition;
        sumError = sumError + error;

        p = error * kp;
        i = sumError * ki;

        setPower(p);
    }


    public boolean inRange(){
        return Math.abs(error) < range;
    }


    //________________________________ Getters and Setters _______________________________//

    public double getTargetPosition() {
        return targetPosition;
    }

    public void setTargetPosition(double position){
        targetPosition = position;
    }

    public double getPosition(){
        return realPosition;
    }

    //__________________________________ String For Telemetry ________________________________//

    public String string(){
        return  String.format("kp: %f\n", kp)+
                String.format("ki: %f\n", ki)+
                String.format("p: %f\n", p)+
                String.format("i: %f\n", i)+
                String.format("Min: %f", min)+
                String.format("Max: %f", max)+
                String.format("Start Position: %f\n", startPosition)+
                String.format("Current Position: %f\n", currentPosition)+
                String.format("Target Position: %f\n", targetPosition)+
                String.format("Real Position: %f\n", realPosition)+
                String.format("error: %f\n", error)+
                String.format("Power: %f\n", p);
    }

}
