package com.bears.zoboomafoo;


/**
 * Created by Alonso on 23/12/2015.
 */
public class AutonomousSetup extends Autonomous{
    long alphaTime;
    long deltaTime;
    int x = 0;

    public int stage = 0;
    int lastSatge=0;

    //AutonomousSetup(Telemetry telemetry){
       // this.telemetry = telemetry;
    //}


    @Override
    public void init() {
        alphaTime = System.currentTimeMillis();

        telemetry.addData("End Point", "");
        telemetry.addData("Alliance", "");
        telemetry.addData("Start Point", "");
        telemetry.addData("Wait", "");
    }

    @Override
    public void start(){
        telemetry.addData("End Point", "");
        telemetry.addData("Alliance", "");
        telemetry.addData("Start Point", "");
        telemetry.addData("Wait", "");

        endPoint = EndPoint.OUR_RAMP;
        alliance = Alliance.BLUE;
    }

    @Override
    public void loop() {

        while((gamepad1.a || gamepad1.b) && lastSatge != stage){}
        lastSatge = stage;


        switch(stage){
            case 0:
                select(Alliance.values().length);
                alliance = Alliance.values()[x];

                blinkData("Alliance", alliance.name());
                telemetry.addData("End Point", "");
                telemetry.addData("Start Point", "");
                telemetry.addData("Wait", "");

                if (gamepad1.a) {
                    telemetry.addData("Alliance", alliance.name());
                    telemetry.addData("End Point", endPoint.name());
                    x=0;
                    //while (gamepad1.a){}

                    stage += 1;
                }
                break;

            case 1:
                select(EndPoint.values().length);
                endPoint = EndPoint.values()[x];

                telemetry.addData("Alliance", alliance.name());
                blinkData("End Point", endPoint.name());
                telemetry.addData("Start Point", "");
                telemetry.addData("Wait", "");

                if (gamepad1.a) {
                    telemetry.addData("End Point", endPoint.name());
                    telemetry.addData("Start Point", String.valueOf(startPoint));
                    x=0;
                    //while (gamepad1.a){}
                    stage += 1;
                }
                if (gamepad1.b) {
                    telemetry.addData("Alliance", "");
                    telemetry.addData("End Point", endPoint.name());
                    x=0;
                    //while (gamepad1.b){}
                    stage -= 1;
                }
                break;



            case 2:
                select(2);
                startPoint = x;

                telemetry.addData("Alliance", alliance.name());
                telemetry.addData("End Point", endPoint.name());
                blinkData("Start Point", String.valueOf(startPoint));
                telemetry.addData("Wait",  "");

                if (gamepad1.a) {
                    telemetry.addData("Start Point", String.valueOf(startPoint));
                    telemetry.addData("Wait", String.valueOf(wait) + " sec");
                    x=0;
                    //while (gamepad1.a){}
                    stage += 1;
                }
                if (gamepad1.b) {
                    telemetry.addData("End Point", "");
                    telemetry.addData("Start Point", String.valueOf(startPoint));
                    x=0;
                    //while (gamepad1.b){}
                    stage -= 1;
                }
                break;



            case 3:
                select(31);
                wait = x;

                telemetry.addData("Alliance", alliance.name());
                telemetry.addData("End Point", endPoint.name());
                telemetry.addData("Start Point", String.valueOf(startPoint));
                blinkData("Wait", String.valueOf(wait) + " sec");

                if (gamepad1.a) {
                    telemetry.addData("Wait", String.valueOf(wait) + " sec");
                    x=0;
                    //while (gamepad1.a){}
                    stage += 1;
                }
                if (gamepad1.b) {
                    telemetry.addData("Start Point", String.valueOf(startPoint));
                    telemetry.addData("Wait", String.valueOf(wait) + " sec");
                    x=0;
                    //while (gamepad1.b){}
                    stage -= 1;
                }
                break;

            case 4:
                if (gamepad1.b) {
                    x=0;
                    telemetry.addData("Alliance", alliance.name());
                    telemetry.addData("End Point", endPoint.name());
                    telemetry.addData("Start Point", String.valueOf(startPoint));
                    telemetry.addData("Wait", String.valueOf(wait) + " sec");
                    //while (gamepad1.a){}
                    stage -= 1;
                }
        }
    }

    void select(int length){
        if (gamepad1.dpad_up){
            x = (x >= length - 1) ? 0 : x + 1;
            while (gamepad1.dpad_up){}
        }

        if (gamepad1.dpad_down){
            x = ( x <= 0 ) ? length - 1 : x - 1;
            while (gamepad1.dpad_down){}
        }
    }


    void blinkData(String key, String value){
        deltaTime = System.currentTimeMillis() - alphaTime;
        if (deltaTime%600<300 || (gamepad1.dpad_down || gamepad1.dpad_up) ){
            telemetry.addData(key, value);
        }
        else{
            telemetry.addData(key, "");
        }
    }



}
