package com.bears.zoboomafoo;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
/**
 * Awesomely created by lord_alonso on 14/12/15.
 */
public class Autonomous extends OpMode{
    Robot zoboomafoo;

    //      AutonomousSetup setup = new AutonomousSetup(telemetry);


    static EndPoint endPoint;
    static Alliance alliance;
    static int startPoint = 0;
    static int wait = 0;

    int mirror;
    int shelterCorrection;
    double servoPosition;

    double distance;
    double yPower;
    double xPower;
    double turnPower;

    int stage;
    int nextStage;

    enum EndPoint{
        OUR_RAMP(0), ANTI_RAMP(45), SHELTER(0);

        int distance;
        EndPoint(int distance){
            this.distance = distance;
        }
    }

    enum Alliance{
        BLUE, RED
    }


    @Override
    public void init() {
        zoboomafoo = new Robot(hardwareMap, gamepad1, gamepad2);
        //zoboomafoo.robotDrive.setPrecisionMultiplier(0.1);
        zoboomafoo.robotDrive.resetDrive();
        //christopherRobin.robotDrive.readyDrive();
        zoboomafoo.lift.init();
        zoboomafoo.dispenser.setPosition(0);

        stage = 0;
        nextStage = 1;

        telemetry.addData("Alliance", alliance.name());
        telemetry.addData("Auto", endPoint.name());
        telemetry.addData("Wait", wait);

        telemetry.addData("Encoder RF Position", zoboomafoo.rightFront.motor.getCurrentPosition());
        telemetry.addData("Encoder RB Position", zoboomafoo.rightBack.motor.getCurrentPosition());
        telemetry.addData("Encoder LF Position", zoboomafoo.leftFront.motor.getCurrentPosition());
        telemetry.addData("Encoder LB Position", zoboomafoo.leftBack.motor.getCurrentPosition());

        if(alliance == Alliance.BLUE){
            mirror = -1;
            shelterCorrection=30;
            servoPosition = 0.45;
        }
        else {
            mirror = 1;
            shelterCorrection=0;
            servoPosition = 0.85;
        }

    }

    @Override
    public void start(){
        waitMilli(wait*1000);
    }

    @Override
    public void loop() {

 //       try { wait(wait * 1000); }
   //     catch (InterruptedException e) { e.printStackTrace(); }

        switch (stage){
            case 0:
                zoboomafoo.robotDrive.resetDrive();
                waitMilli(500);
                stage = nextStage;
                break;
            case 1:
                zoboomafoo.robotDrive.driveToPositionDefault(45 + (startPoint*60), -0.3, 0*mirror);
                break;
            case 2:
                zoboomafoo.robotDrive.driveToPositionDefault(14, 0, 0.6*mirror);
                break;
            case 3:
                zoboomafoo.robotDrive.driveToPositionDefault(186 - (startPoint*60*1.4142) - shelterCorrection, -0.4, 0*mirror);
                break;
            case 4:
                zoboomafoo.robotDrive.driveToPositionDefault(18, 0, 0.6*mirror);
                break;
            case 5:
                zoboomafoo.robotDrive.driveToPositionDefault(60+shelterCorrection/2, -0.3, 0*mirror);
                break;


            //--------------------------------  Ramp  -----------------------------------//

            case 6:
                zoboomafoo.robotDrive.driveToPositionDefault(60 + endPoint.distance, 0.3, 0*mirror);
                break;
            case 7:
                zoboomafoo.robotDrive.driveToPositionDefault(32, 0, -0.6*mirror);
                break;
            case 8:
                zoboomafoo.robotDrive.driveToPositionDefault(80 - shelterCorrection + endPoint.distance, 0.3, 0 * mirror);
                break;
            case 9:
                zoboomafoo.robotDrive.driveToPositionDefault(15, 0, -0.6*mirror);
                break;
            case 10:
                zoboomafoo.robotDrive.driveToPositionDefault(60, 0.4, 0 * mirror);
                break;


            //--------------------------------  Shelter  -----------------------------------//

            case 12:
                zoboomafoo.robotDrive.driveToPositionDefault(32, 0, -0.6 * mirror);
                break;
            case 13:
                zoboomafoo.robotDrive.driveToPositionDefault(45, 0.3, 0*mirror);
                break;



            default:
                zoboomafoo.robotDrive.drive(0, 0, 0);
                //christopherRobin.lift.reset();
                break;

        }


        zoboomafoo.lift.updatePosition();
        zoboomafoo.lift.moveToPosition();

        if (zoboomafoo.robotDrive.gotToPosition && zoboomafoo.lift.inRange()) {
            stage=0;
            nextStage += 1;

            if (nextStage==6 && endPoint == EndPoint.SHELTER)
                nextStage = 12;

        }

        telemetry.addData("Encoder RF Position", zoboomafoo.rightFront.motor.getCurrentPosition());
        telemetry.addData("Encoder RB Position", zoboomafoo.rightBack.motor.getCurrentPosition());
        telemetry.addData("Encoder LF Position", zoboomafoo.leftFront.motor.getCurrentPosition());
        telemetry.addData("Encoder LB Position", zoboomafoo.leftBack.motor.getCurrentPosition());
        telemetry.addData("Drive in Position", zoboomafoo.robotDrive.gotToPosition);
        telemetry.addData("Lift in Range", zoboomafoo.lift.inRange());
        telemetry.addData("Stage", stage);
        telemetry.addData("Slow Toggled", zoboomafoo.robotDrive.slowToggled);

    }

    void waitMilli(int milliseconds){
        long startTime = System.currentTimeMillis();
        long deltaTime= System.currentTimeMillis() - startTime;

        while(deltaTime < milliseconds){
            deltaTime= System.currentTimeMillis() - startTime;
            telemetry.addData("Delta Time", deltaTime);
        }

    }
}
