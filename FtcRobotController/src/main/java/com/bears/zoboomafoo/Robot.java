package com.bears.zoboomafoo;

import com.bears.hardware.BearsMotor;
import com.bears.hardware.Drive;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

/**
 * Awesomely created by lord_alonso on 14/12/15.
 */
public class Robot {

   // public static Robot instance = new Robot();
   // public static Alliance alliance;

    public Drive robotDrive = null;
    public Lift lift = null;
    public Feeder feeder = null;

    public BearsMotor rightFront = null;
    public BearsMotor rightBack = null;
    public BearsMotor leftFront = null;
    public BearsMotor leftBack = null;

    public BearsMotor feederMotor = null;
    public BearsMotor liftMotor1 = null;
    public BearsMotor liftMotor2 = null;

    public Servo dispenser = null;

    public DigitalChannel limitSwitch;

    Robot(HardwareMap hardwareMap, Gamepad gamepad1, Gamepad gamepad2){
        rightFront = new BearsMotor(hardwareMap.dcMotor.get("rf"), BearsMotor.Type.NEVEREST40);
        rightBack = new BearsMotor(hardwareMap.dcMotor.get("rb"), BearsMotor.Type.NEVEREST40);
        leftFront = new BearsMotor(hardwareMap.dcMotor.get("lf"), BearsMotor.Type.NEVEREST40);
        leftBack = new BearsMotor(hardwareMap.dcMotor.get("lb"), BearsMotor.Type.NEVEREST40);
        robotDrive = new Drive(rightFront, rightBack,leftFront, leftBack, gamepad1, 0, true, false, 2, 50/54d);

        feederMotor = new BearsMotor(hardwareMap.dcMotor.get("f"), BearsMotor.Type.NEVEREST40);
        feederMotor.motor.setDirection(DcMotor.Direction.REVERSE);
        feeder =  new Feeder(feederMotor, null);

        liftMotor1 = new BearsMotor(hardwareMap.dcMotor.get("l"), BearsMotor.Type.NEVEREST60);
        lift = new Lift(liftMotor1, liftMotor2, 0, 18000);

        dispenser = hardwareMap.servo.get("d");

        limitSwitch = hardwareMap.digitalChannel.get("limitSwitch");
    }



    //public void defineStaticInstance(){
      //  instance = new Robot();
    //}


}
