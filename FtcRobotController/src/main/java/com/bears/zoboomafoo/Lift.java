package com.bears.zoboomafoo;

import com.bears.hardware.BearsMotor;
import com.bears.hardware.PositionControl;

/**
 * Created by Alonso on 13/01/2016.
 */
public class Lift extends PositionControl {

    public Lift(BearsMotor motor1, BearsMotor motor2) {
        super(motor1, motor2);
    }

    public Lift(BearsMotor motor1, BearsMotor motor2, double min, double max) {
        super(motor1, motor2, min, max);
    }

    public enum Position{
        BOTTOM(0),
        LOW_GOAL(3000),
        MID_GOAL(6000),
        HIGH_GOAL(9500);


        double distance;

        Position(double distance){
            this.distance = distance;
        }
    }

    public void setPosition(Position position) {
         setTargetPosition(position.distance);
    }


}
