package com.bears.zoboomafoo;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.AnalogOutput;
import com.qualcomm.robotcore.hardware.DigitalChannelController;


/**
 * Awesomely created by lord_alonso on 14/12/15.
 */
public class TeleOp extends OpMode{
    Robot zoboomafoo;
    boolean hookChange;
    boolean precitionChange;



    @Override
    public void init() {
        telemetry.addData("test", 0);
        zoboomafoo = new Robot(hardwareMap, gamepad1, gamepad2);
        zoboomafoo.lift.init();
        zoboomafoo.dispenser.setPosition(0);
        zoboomafoo.limitSwitch.setMode(DigitalChannelController.Mode.INPUT);
    }


    public void start() {}



    @Override
    public void loop() {

        ////////////////////////////////////////// Drive Functions //////////////////////////////////////////////////

        zoboomafoo.robotDrive.arcadeDrive();
        telemetry.addData("Drive", zoboomafoo.robotDrive.string());


        if(gamepad1.start){
            if(precitionChange){
                zoboomafoo.robotDrive.togglePrecision();
                precitionChange = false;
            }
        } else {
            precitionChange = true;
        }



        ////////////////////////////////////////// Feeder Functions //////////////////////////////////////////////////


        zoboomafoo.feeder.setPower(gamepad2.left_trigger - gamepad2.right_trigger);


        ////////////////////////////////////////// Dispenser Functions //////////////////////////////////////////////////

        if(gamepad2.dpad_right || gamepad2.dpad_left)
            zoboomafoo.dispenser.setPosition(0.65);

        else
            zoboomafoo.dispenser.setPosition(0);


        ////////////////////////////////////////// Lift Functions //////////////////////////////////////////////////


        zoboomafoo.lift.updatePosition();

        if (gamepad2.start || zoboomafoo.limitSwitch.getState())
            zoboomafoo.lift.reset();

        if (gamepad2.start)
            zoboomafoo.lift.safety = false;
        else
            zoboomafoo.lift.safety = true;



        if(gamepad2.a)
            zoboomafoo.lift.setPosition(Lift.Position.BOTTOM);

        if(gamepad2.b)
            zoboomafoo.lift.setPosition(Lift.Position.LOW_GOAL);

        if(gamepad2.x)
            zoboomafoo.lift.setPosition(Lift.Position.MID_GOAL);

        if (gamepad2.y)
            zoboomafoo.lift.setPosition(Lift.Position.HIGH_GOAL);



        if (gamepad2.right_bumper) {
            zoboomafoo.lift.setPower(.8);
            zoboomafoo.lift.setTargetPosition(zoboomafoo.lift.getPosition());
        }
        else if (gamepad2.left_bumper) {
            zoboomafoo.lift.setPower(-.8);
            zoboomafoo.lift.setTargetPosition(zoboomafoo.lift.getPosition());
        }
        else
            zoboomafoo.lift.moveToPosition();

        telemetry.addData("Lift Info", zoboomafoo.lift.string());
        telemetry.addData("Limit Switch", zoboomafoo.limitSwitch.getState());
    }
}
