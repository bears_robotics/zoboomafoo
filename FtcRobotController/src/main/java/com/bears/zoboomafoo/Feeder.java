package com.bears.zoboomafoo;

import com.bears.hardware.BearsMotor;
import com.bears.hardware.TwoWayControl;

/**
 * Created by Alonso on 13/01/2016.
 */
public class Feeder extends TwoWayControl{

    public Feeder(BearsMotor motor1, BearsMotor motor2) {
        super(motor1, motor2);
    }

}
